import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  totalPrice: number=0;

  title(title: any) {
      throw new Error('Method not implemented.');
  }
  constructor(private cartService: CartService) {
    this.totalPrice = 0;
  }

  get cartItems() {
    return this.cartService.getCartItems();
    

  }

  getTotalPrice() {
    let totalPrice = 0;
    const cartItems = this.cartService.getCartItems();
  
    for (const item of cartItems) {
      totalPrice += item.price * item.quantity;
    }
  
    return totalPrice;
  }
  
  ngOnInit() {}
}

