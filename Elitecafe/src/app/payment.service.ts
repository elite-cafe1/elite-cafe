import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  private apiUrl = 'http://your-backend-api-url/payment';

  constructor(private http: HttpClient) {}

  processPayment(paymentDetails: any): Observable<any> {
    return this.http.post<any>(this.apiUrl, paymentDetails);
  }
}
