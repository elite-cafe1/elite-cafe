import { Component } from '@angular/core';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrl: './qrcode.component.css'
})
export class QrcodeComponent {
  title = 'Project';

  myAngularxQrCode:any;
  constructor(){
    this.myAngularxQrCode = 'your QR code data string';
  }

}
