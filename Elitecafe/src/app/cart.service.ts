import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  cartItems: any[] = [];

  addToCart(product: any) {
    this.cartItems.push({ ...product, quantity: 1 });
  }

  getCartItems() {
    return this.cartItems;
  }

  getTotalAmount() {
    let total = 0;
    this.cartItems.forEach(item => {
      total += item.price * item.quantity;
    });
    return total;
  }
}
