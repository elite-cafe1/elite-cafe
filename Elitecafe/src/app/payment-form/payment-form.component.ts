import { Component } from '@angular/core';
import { PaymentService } from '../payment.service';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrl: './payment-form.component.css'
})
export class PaymentFormComponent {
  cardNumber: string = '';
  expiryDate: string = '';
  cvv: string = '';

  constructor(private paymentService: PaymentService) {}

  processPayment() {
    // Call payment service method to initiate payment
    this.paymentService.processPayment({
      cardNumber: this.cardNumber,
      expiryDate: this.expiryDate,
      cvv: this.cvv
    }).subscribe(response => {
      // Handle payment response
      console.log(response);
    });
  }
}