package com.model;

//Booking.java

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Booking {

 @Id
 @GeneratedValue(strategy = GenerationType.IDENTITY)
 private Long id;
private String customerName;
 private String country;
 private Date dob;
 private String startTime;
 private String endTime;
 private String phonenumber;
 private String emailId;


public Booking(Long id, String customerName, String country, Date dob, String startTime, String endTime,
		String phonenumber, String emailId) {
	super();
	this.id = id;
	this.customerName = customerName;
	this.country = country;
	this.dob = dob;
	this.startTime = startTime;
	this.endTime = endTime;
	this.phonenumber = phonenumber;
	this.emailId = emailId;
}

 
 public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public Date getDob() {
	return dob;
}
public void setDob(Date dob) {
	this.dob = dob;
}
public String getStartTime() {
	return startTime;
}
public void setStartTime(String startTime) {
	this.startTime = startTime;
}
public String getEndTime() {
	return endTime;
}
public void setEndTime(String endTime) {
	this.endTime = endTime;
}
public String getPhonenumber() {
	return phonenumber;
}
public void setPhonenumber(String phonenumber) {
	this.phonenumber = phonenumber;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
 // Getters and setters
 // Constructors
 // Other methods
}
