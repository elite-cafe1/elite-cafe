package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Customer {
    @Id
    @GeneratedValue
    @Column(unique = true)
    private int customerId;
    private String customerName;
    private String gender;
    private String country;
    
	@NotNull
	@Column(name = "phonenumber", unique = true, nullable = false)
	private String phonenumber;
    
    @NotNull
    @Column(name = "emailId", unique = true, nullable = false)
	private String emailId;
	private String password;
	private String otp;

  
    public Customer() {
       
    }
    public Customer(String customerName, String gender, @NotNull String phoneNumber, @NotNull String emailId,String country,Date doj,String countryCode,
			String password, String otp) {
		
		this.customerName = customerName;
		this.gender = gender;
		this.country = country;
		this.phonenumber = phoneNumber;
		this.emailId = emailId;
		this.password = password;
		this.otp = otp;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
}


