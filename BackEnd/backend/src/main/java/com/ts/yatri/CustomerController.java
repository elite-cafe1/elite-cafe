package com.ts.yatri;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDao;
//import com.model.Credentials;
import com.model.Customer;


@CrossOrigin(origins="http://localhost:4200")
@RestController
public class CustomerController {
	
	@Autowired
	CustomerDao customerDao;
	
	@GetMapping("getAllCustomers")
	public List<Customer> getAllCustomers(){
		return customerDao.getAllCustomers();
	}
	
	@PostMapping("addCustomer")
	public Customer addCustomer(@RequestBody Customer customer) {
		return customerDao.addCustomer(customer);
	}
	
	@GetMapping("customerLogin/{emailId}/{password}")
	public Customer customerLogin(@PathVariable String emailId, @PathVariable String password) {
		return customerDao.customerLogin(emailId, password);
	}
	
	@DeleteMapping("deleteCustomerById/{customerId}")
	public String deleteCustomerById(@PathVariable int customerId) {
		customerDao.deleteCustomerById(customerId);
		return "Employee with empId:" + customerId + " Deleted Successfully!!!";
	}
	
	@GetMapping("getCustomerEmailId/{emailId}")
	public Customer getCustomerEmailId(@PathVariable String emailId, @PathVariable String password) {
		return customerDao.getCustomerEmailId(emailId);
	}
	
//	@PutMapping("updateCustomerPassword")
//	public Customer updateCustomerPassword(@RequestBody Credentials cred) {
//		System.out.println(cred.getCustomerEmailId());
//		System.out.println(cred.getCustomerPassword());
//		
//		return customerDao.updateCustomerPassword(cred.getCustomerEmailId(),cred.getCustomerPassword());
//	}
}