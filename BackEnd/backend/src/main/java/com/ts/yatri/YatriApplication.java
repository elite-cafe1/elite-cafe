package com.ts.yatri;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.model.TwilioConfig;
import com.twilio.Twilio;
import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.model.CorsConfig;
import com.model.TwilioConfig;

@EnableJpaRepositories(basePackages="com.dao")
@EntityScan(basePackages="com.model")
@SpringBootApplication(scanBasePackages="com")
@EnableConfigurationProperties
@Import({TwilioConfig.class, CorsConfig.class})
public class YatriApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(YatriApplication.class, args);
	}

}
