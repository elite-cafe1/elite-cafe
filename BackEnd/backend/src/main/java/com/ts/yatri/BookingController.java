package com.ts.yatri;

//BookingController.java

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BookingService;
import com.model.Booking;

@RestController
public class BookingController {

 private final BookingService bookingService;

 @Autowired
 public BookingController(BookingService bookingService) {
     this.bookingService = bookingService;
 }

 @PostMapping("/api/bookings")
 public Booking bookTable(@RequestBody Booking booking) {
     return bookingService.saveBooking(booking);
 }
}
