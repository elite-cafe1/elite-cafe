package com.dao;

import java.util.List;

import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.model.Customer;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;

@Service
@Transactional
public class CafeDAO {
    
    @Autowired
    private CafeRepository customerRepository;
    
    @Autowired
    private JavaMailSender mailSender;
    
    private static final int OTP_LENGTH = 6;
    private static final String ACCOUNT_SID = "ACa623242705648a9f3e5076567d4d591c";
    private static final String AUTH_TOKEN = "778bc229512d88883533b3b72c73350c";
    private static final String TWILIO_PHONE_NUMBER = "+17853673066";

    static {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }
    
    public Customer customerLogin(String emailId, String password) {
        Customer customer = customerRepository.findByEmailId(emailId);
        if (customer != null) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            if (passwordEncoder.matches(password, customer.getPassword())) {
                return customer;
            }
        }
        return null;
    }

    public Customer addCustomer(Customer customer) {
        String otp = generateOTP();
        customer.setOtp(otp);
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(customer.getPassword());
        customer.setPassword(encryptedPwd);
        Customer savedCustomer = customerRepository.save(customer);
        sendWelcomeEmail(savedCustomer);
        sendOtpViaSms(savedCustomer);
        return savedCustomer;
    }
    
    private void sendWelcomeEmail(Customer customer) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(customer.getEmailId());
        message.setSubject("Welcome to our website");
        message.setText("Dear " + customer.getCusName() + ",\n\n" + "Thank you for registration ");
        mailSender.send(message);
    }
     
    public Customer getCustomerByEmailId(String emailId) {
        Customer customer = customerRepository.findByEmailId(emailId);
        if (customer != null) {
            String otp = generateOTP();
            customer.setOtp(otp);
            sendWelcomeEmail(customer);
        }
        return customer;
    }

    private String generateOTP() {
        Random random = new Random();
        StringBuilder otp = new StringBuilder();
        for (int i = 0; i < OTP_LENGTH; i++) {
            otp.append(random.nextInt(10));
        }
        return otp.toString();
    }

//    private void sendOtpViaSms(Customer customer) {
//        try {
//            Message message = Message.creator(
//                new com.twilio.type.PhoneNumber(customer.getPhonenumber()),
//                new com.twilio.type.PhoneNumber(TWILIO_PHONE_NUMBER),
//                "Your OTP for registration is: " + customer.getOtp()
//            ).create();
//            System.out.println("OTP sent successfully via SMS.");
//        } catch (ApiException e) {
//            if (e.getCode() == 21614) {
//                System.err.println("OTP not sent: Twilio trial accounts cannot send messages to unverified numbers.");
//            } else {
//                System.err.println("Error sending OTP via SMS: " + e.getMessage());
//            }
//        }
//    }

    public void deleteCustomerById(int customerId) {
        customerRepository.deleteById(customerId);
    }
}
